/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('farmatodoApp')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('apikey', '5e1a1ac97631eeac4253875bbf6a1906')
    .constant('hash', '1e7e56f36f436ba0f173f381af62f131')
    .constant('keyFavoritos', 'favoritos')
    .constant('ts', '1522342488');

})();
