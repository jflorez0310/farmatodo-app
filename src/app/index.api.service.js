(function (angular) {
    'use strict';

    angular
        .module('farmatodoApp')
        .factory('api', [
                '$http',
                 'apikey',
                 'hash',
                 'ts',
                 api]);

    function api($http, apikey, hash, ts)
    {

        var vm = {};
        vm.findAll = findAll;
        vm.findByName = findByName;
        vm.findComic = findComic;

        return vm;


        /////////////////////////////////////////////////////////

        function findAll() {
            var parametros = { params: { limit: 10 }};
            var url = "http://gateway.marvel.com/v1/public/characters?ts="+ts+"&apikey="+apikey+"&hash="+hash;
            return $http.get(url, parametros);
        }

        //////////////////////////////// request GENERAL's subUrl ////////////////////////////////
        function findByName(name)
        {
            var parametros = { params: { nameStartsWith: name , limit: 10 }};
            var url = "http://gateway.marvel.com/v1/public/characters?ts="+ts+"&apikey="+apikey+"&hash="+hash;
            return $http.get(url, parametros);
        }

        function findComic(param){
            var url = param+"?ts="+ts+"&apikey="+apikey+"&hash="+hash;
            return $http.get(url);
        }

    }
})(window.angular);
