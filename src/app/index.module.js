(function() {
  'use strict';

  angular
    .module('farmatodoApp', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'toastr', 'ui.bootstrap']);

})(window.angular);
