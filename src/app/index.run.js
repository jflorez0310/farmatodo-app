(function() {
  'use strict';

  angular
    .module('farmatodoApp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
