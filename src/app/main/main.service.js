(function (angular) {
    'use strict';

    angular
        .module('farmatodoApp')
        .factory('mainService', [
            'mainDataService',
            'favoriteFactory',
            'keyFavoritos',
            '$filter',
            mainService
        ]);

    function mainService(mainDataService, favoriteFactory, keyFavoritos, $filter)
    {
        var vm = {};

        //injects

        //data
        vm.resultado = {};
        vm.mainDataService = mainDataService;
        vm.favoriteFactory = favoriteFactory;
        vm.favoritos = vm.favoriteFactory.getObject(keyFavoritos, []);

        //Methods
        vm.findAll = findAll;
        vm.findByName = findByName;
        vm.findComic = findComic;
        vm.saveFavorite = saveFavorite;
        vm.deleteFavorito = deleteFavorito;

        return vm;

        //////////////////////////////////////////////////////////////////////////////////////////////////

        function findAll(){
            vm.mainDataService.findAll().then(function(response){
               vm.resultado = response;
            });
        }

        function findByName(name){
            vm.mainDataService.findByName(name).then(function(response){
                vm.resultado = response;
            })
        }

        function findComic(param){
            return vm.mainDataService.findComic(param).then(function (response){
                return response;
            })
        }


        function saveFavorite(objeto){
            var obj = $filter('filter')(vm.favoritos, {id: objeto.id}, true)[0];
            console.log('obj', obj)
            if(obj === undefined){
                vm.favoritos.push(objeto);
                vm.favoriteFactory.setObject(keyFavoritos, vm.favoritos);
            }

        }

        function deleteFavorito(index){
            vm.favoritos.splice(index, 1);
            vm.favoriteFactory.setObject(keyFavoritos, vm.favoritos);
        }

    }
})(window.angular);
