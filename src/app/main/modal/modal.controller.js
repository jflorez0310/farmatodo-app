(function() {
  'use strict';

  angular
    .module('farmatodoApp')
    .controller('ModalController', [
      'mainService',
      'constantParam',
      '$uibModalInstance',
      ModalController
    ]);

  /** @ngInject */
  function ModalController(mainService, constantParam, $uibModalInstance) {
    var vm = this;

    // Variables
    vm.data = {};

    // Methods
    vm.closeModal = closeModal;
    vm.addFavorite = addFavorite;

    loadComic();



    /////////////////////////////////////////////////////////////////

    function loadComic(){
      mainService.findComic(constantParam.resourceURI).then(function(response){
        vm.data = response;
        console.log('data', vm.data);
      })
    }


    function closeModal(){
      $uibModalInstance.close();
    }

    function addFavorite(objeto){
      console.log('add favorite', objeto);
      mainService.saveFavorite(objeto);
    }
  }
})(window.angular);
