(function() {
  'use strict';

  angular
    .module('farmatodoApp')
    .controller('MainController', [
     'mainService',
      '$scope',
      '$uibModal',
      MainController
    ]);

  /** @ngInject */
  function MainController(mainService, $scope, $uibModal) {
    var vm = this;

    // Variables
    vm.name = "";

    vm.mainService = mainService;

    // Methods
    vm.openModal = openModal;
    vm.deleteFavorito = deleteFavorito;

    vm.mainService.findAll();
    /////////////////////////////////////

    function openModal(param){
      $uibModal.open({
        animation: true,
        templateUrl: 'app/main/modal/modal.html',
        controller: 'ModalController as vm',
        size: 'md',
        resolve : {
          constantParam : param
        }
      });
    }


    function deleteFavorito(index){
      vm.mainService.deleteFavorito(index);
    }

    $scope.$watch(function(){
      return vm.name;
    },function(response){
      mainService.findByName(vm.name);
    }, true);


  }
})(window.angular);
