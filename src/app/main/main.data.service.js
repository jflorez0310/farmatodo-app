(function (angular) {
  'use strict';

  angular
    .module('farmatodoApp')
    .service('mainDataService', [
      'api',
      '$q',
      mainDataService
    ]);

  function mainDataService(api, $q) {
    var vm = this;


    // Variabls


    //methods

    vm.findAll = findAll;
    vm.findByName = findByName;
    vm.findComic = findComic;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Metodo que consulta todos los personajes
    function findAll(){
      return api.findAll()
        .then(function(response){
            return response.data.data;
        })
        .catch(function(err){
            return $q.reject(err);
        });
    }

    // Metodo que consulta los personajes por nombre
    function findByName(name){
      return api.findByName(name)
        .then(function(response){
          return response.data.data;
        })
        .catch(function (err){
            return $q.reject(err);
        });
    }

    //Metodo que consulta un comic por un id
    function findComic(param){
        return api.findComic(param)
            .then(function(response){
                return response.data.data.results[0];
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
  }
})(window.angular);
