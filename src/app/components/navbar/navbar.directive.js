(function() {
  'use strict';

  angular
    .module('farmatodoApp')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          nameSearch: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(mainService) {
      var vm = this;

      vm.buscar = buscar;

      function buscar(){
        mainService.findByName(vm.name);
      }
    }
  }

})(window.angular);
